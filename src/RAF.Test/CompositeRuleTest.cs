﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Ploeh.AutoFixture;
namespace RAF.Test
{
    [TestClass]
    public class CompositeRuleTest
    {
        private Fixture _fixture = new Fixture();

        [TestMethod]
        public void Test_calls_test_on_the_rules_conditions()
        {
            var rule = new CompositeRule<DummyWorld, DummySubject, DummyTarget>(
                _fixture.Create<string>(),
                Mock.Of<ICompositeRuleAction<DummySubject, DummyTarget>>()
                );

            var mockCondition = new Mock<ICompositeRuleCondition<DummySubject, DummyTarget>>();
            rule.AddCondition(mockCondition.Object);

            var world = new DummyWorld();
            world.Targets = _fixture.CreateMany<DummyTarget>();
            var subject = new DummySubject();

            rule.Test(world, subject);

            mockCondition.Verify(x => x.Test(It.IsAny<IEnumerable<DummyTarget>>(), subject), Times.AtLeastOnce);
        }

        [TestMethod]
        public void Test_returns_successful_result_if_all_conditions_pass()
        {
            var world = new DummyWorld();
            var subject = new DummySubject();

            var mockAction = new Mock<ICompositeRuleAction<DummySubject, DummyTarget>>();

            var ruleName = _fixture.Create<string>();

            var rule = new CompositeRule<DummyWorld, DummySubject, DummyTarget>(
                ruleName,
                mockAction.Object);

            var target = new DummyTarget();
            var targets = new DummyTarget[] { target };

            world.Targets = targets;

            var mockCondition = new Mock<ICompositeRuleCondition<DummySubject, DummyTarget>>();
            mockCondition.Setup(x => x.Test(It.IsAny<IEnumerable<DummyTarget>>(), subject)).Returns(targets);
            rule.AddCondition(mockCondition.Object);

            mockCondition = new Mock<ICompositeRuleCondition<DummySubject, DummyTarget>>();
            mockCondition.Setup(x => x.Test(It.IsAny<IEnumerable<DummyTarget>>(), subject)).Returns(targets);
            rule.AddCondition(mockCondition.Object);

            mockCondition = new Mock<ICompositeRuleCondition<DummySubject, DummyTarget>>();
            mockCondition.Setup(x => x.Test(It.IsAny<IEnumerable<DummyTarget>>(), subject)).Returns(targets);
            rule.AddCondition(mockCondition.Object);


            var result = rule.Test(world, subject);

            Assert.AreEqual(result.RuleName, ruleName);
            Assert.IsTrue(result.Passed);
        }

        [TestMethod]
        public void Test_calls_perform_on_its_action_if_all_conditions_pass()
        {
            var world = new DummyWorld();
            var subject = new DummySubject();

            var mockAction = new Mock<ICompositeRuleAction<DummySubject, DummyTarget>>();

            var rule = new CompositeRule<DummyWorld, DummySubject, DummyTarget>(
                _fixture.Create<string>(),
                mockAction.Object);

            var target = new DummyTarget();
            var targets = new DummyTarget[] { target };

            world.Targets = targets;

            var mockCondition = new Mock<ICompositeRuleCondition<DummySubject, DummyTarget>>();
            mockCondition.Setup(x => x.Test(It.IsAny<IEnumerable<DummyTarget>>(), subject)).Returns(targets);
            rule.AddCondition(mockCondition.Object);

            mockCondition = new Mock<ICompositeRuleCondition<DummySubject, DummyTarget>>();
            mockCondition.Setup(x => x.Test(It.IsAny<IEnumerable<DummyTarget>>(), subject)).Returns(targets);
            rule.AddCondition(mockCondition.Object);

            mockCondition = new Mock<ICompositeRuleCondition<DummySubject, DummyTarget>>();
            mockCondition.Setup(x => x.Test(It.IsAny<IEnumerable<DummyTarget>>(), subject)).Returns(targets);
            rule.AddCondition(mockCondition.Object);


            rule.Test(world, subject);

            mockAction.Verify(x => x.Perform(targets, subject));
        }

        [TestMethod]
        public void Test_returns_failed_result_if_any_conditions_fail()
        {
            var world = new DummyWorld();
            var subject = new DummySubject();

            var mockAction = new Mock<ICompositeRuleAction<DummySubject, DummyTarget>>();

            var ruleName = _fixture.Create<string>();

            var rule = new CompositeRule<DummyWorld, DummySubject, DummyTarget>(
                ruleName,
                mockAction.Object);

            var target = new DummyTarget();
            var targets = new DummyTarget[] { target };

            world.Targets = targets;

            var failingConditionName = _fixture.Create<string>();

            var mockCondition = new Mock<ICompositeRuleCondition<DummySubject, DummyTarget>>();
            mockCondition.Setup(x => x.Test(It.IsAny<IEnumerable<DummyTarget>>(), subject))
                .Returns(new DummyTarget[0]);
            mockCondition.Setup(x => x.FriendlyName).Returns(failingConditionName);
            rule.AddCondition(mockCondition.Object);

            mockCondition = new Mock<ICompositeRuleCondition<DummySubject, DummyTarget>>();
            mockCondition.Setup(x => x.Test(It.IsAny<IEnumerable<DummyTarget>>(), subject))
                .Returns(targets);
            rule.AddCondition(mockCondition.Object);

            mockCondition = new Mock<ICompositeRuleCondition<DummySubject, DummyTarget>>();
            mockCondition.Setup(x => x.Test(It.IsAny<IEnumerable<DummyTarget>>(), subject))
                .Returns(targets);
            rule.AddCondition(mockCondition.Object);


            var result = rule.Test(world, subject);

            Assert.IsFalse(result.Passed);
            Assert.AreEqual(result.RuleName, ruleName);
            Assert.AreEqual(result.Reason, failingConditionName);
        }

        [TestMethod]
        public void Test_does_not_call_perform_on_its_action_if_if_any_conditions_fail()
        {
            var world = new DummyWorld();
            var subject = new DummySubject();

            var mockAction = new Mock<ICompositeRuleAction<DummySubject, DummyTarget>>();

            var rule = new CompositeRule<DummyWorld, DummySubject, DummyTarget>(
                _fixture.Create<string>(),
                mockAction.Object);

            var target = new DummyTarget();
            var targets = new DummyTarget[] { target };

            world.Targets = _fixture.CreateMany<DummyTarget>();

            var mockCondition1 = new Mock<ICompositeRuleCondition<DummySubject, DummyTarget>>();
            mockCondition1.Setup(x => x.Test(It.IsAny<IEnumerable<DummyTarget>>(), subject))
                .Returns(targets);
            rule.AddCondition(mockCondition1.Object);

            var mockCondition2 = new Mock<ICompositeRuleCondition<DummySubject, DummyTarget>>();
            mockCondition2.Setup(x => x.Test(It.IsAny<IEnumerable<DummyTarget>>(), subject))
                .Returns(new DummyTarget[0]);
            rule.AddCondition(mockCondition2.Object);

            var mockCondition3 = new Mock<ICompositeRuleCondition<DummySubject, DummyTarget>>();
            mockCondition3.Setup(x => x.Test(It.IsAny<IEnumerable<DummyTarget>>(), subject))
                .Returns(targets);
            rule.AddCondition(mockCondition3.Object);


            rule.Test(world, subject);

            mockAction.Verify(x => x.Perform(It.IsAny<IEnumerable<DummyTarget>>(), subject), Times.Never);
        }

        [TestMethod]
        public void Test_does_not_call_test_on_conditions_that_come_after_a_failing_condition()
        {
            var world = new DummyWorld();
            var subject = new DummySubject();

            var mockAction = new Mock<ICompositeRuleAction<DummySubject, DummyTarget>>();

            var rule = new CompositeRule<DummyWorld, DummySubject, DummyTarget>(
                _fixture.Create<string>(),
                mockAction.Object);

            var target = new DummyTarget();
            var targets = new DummyTarget[] { target };

            world.Targets = targets;

            var mockCondition1 = new Mock<ICompositeRuleCondition<DummySubject, DummyTarget>>();
            mockCondition1.Setup(x => x.Test(It.IsAny<IEnumerable<DummyTarget>>(), subject))
                .Returns(targets);
            rule.AddCondition(mockCondition1.Object);

            var mockCondition2 = new Mock<ICompositeRuleCondition<DummySubject, DummyTarget>>();
            mockCondition2.Setup(x => x.Test(It.IsAny<IEnumerable<DummyTarget>>(), subject))
                .Returns(new DummyTarget[0]);
            rule.AddCondition(mockCondition2.Object);

            var mockCondition3 = new Mock<ICompositeRuleCondition<DummySubject, DummyTarget>>();
            mockCondition3.Setup(x => x.Test(It.IsAny<IEnumerable<DummyTarget>>(), subject))
                .Returns(targets);
            rule.AddCondition(mockCondition3.Object);


            rule.Test(world, subject);

            mockCondition3.Verify(x => x.Test(It.IsAny<IEnumerable<DummyTarget>>(), subject), Times.Never);
        }

        [TestMethod]
        public void Test_does_not_call_any_conditions_or_action_if_source_returns_no_targets()
        {
            var world = new DummyWorld();
            var subject = new DummySubject();

            var mockAction = new Mock<ICompositeRuleAction<DummySubject, DummyTarget>>();

            var rule = new CompositeRule<DummyWorld, DummySubject, DummyTarget>(
                _fixture.Create<string>(),
                mockAction.Object);

            var target = new DummyTarget();
            var targets = new DummyTarget[] { target };

            world.Targets = new DummyTarget[0];

            var mockCondition1 = new Mock<ICompositeRuleCondition<DummySubject, DummyTarget>>();
            mockCondition1.Setup(x => x.Test(It.IsAny<IEnumerable<DummyTarget>>(), subject))
                .Returns(targets);
            rule.AddCondition(mockCondition1.Object);

            var mockCondition2 = new Mock<ICompositeRuleCondition<DummySubject, DummyTarget>>();
            mockCondition2.Setup(x => x.Test(It.IsAny<IEnumerable<DummyTarget>>(), subject))
                .Returns(targets);
            rule.AddCondition(mockCondition2.Object);

            var mockCondition3 = new Mock<ICompositeRuleCondition<DummySubject, DummyTarget>>();
            mockCondition3.Setup(x => x.Test(It.IsAny<IEnumerable<DummyTarget>>(), subject))
                .Returns(targets);
            rule.AddCondition(mockCondition3.Object);


            rule.Test(world, subject);

            mockCondition1.Verify(x => x.Test(It.IsAny<IEnumerable<DummyTarget>>(), subject), Times.Never);
            mockCondition2.Verify(x => x.Test(It.IsAny<IEnumerable<DummyTarget>>(), subject), Times.Never);
            mockCondition3.Verify(x => x.Test(It.IsAny<IEnumerable<DummyTarget>>(), subject), Times.Never);

            mockAction.Verify(x => x.Perform(It.IsAny<IEnumerable<DummyTarget>>(), subject), Times.Never);
        }

        [TestMethod]
        public void Test_calls_perform_on_action_with_result_of_last_condition()
        {
            var world = new DummyWorld();
            var subject = new DummySubject();

            var mockAction = new Mock<ICompositeRuleAction<DummySubject, DummyTarget>>();

            var rule = new CompositeRule<DummyWorld, DummySubject, DummyTarget>(
                _fixture.Create<string>(),
                mockAction.Object);

            var targets = _fixture.CreateMany<DummyTarget>();

            world.Targets = targets;

            var expectedTargets = _fixture.CreateMany<DummyTarget>();

            var mockCondition1 = new Mock<ICompositeRuleCondition<DummySubject, DummyTarget>>();
            mockCondition1.Setup(x => x.Test(It.IsAny<IEnumerable<DummyTarget>>(), subject))
                .Returns(targets);
            rule.AddCondition(mockCondition1.Object);

            var mockCondition2 = new Mock<ICompositeRuleCondition<DummySubject, DummyTarget>>();
            mockCondition2.Setup(x => x.Test(It.IsAny<IEnumerable<DummyTarget>>(), subject))
                .Returns(targets);
            rule.AddCondition(mockCondition2.Object);

            var mockCondition3 = new Mock<ICompositeRuleCondition<DummySubject, DummyTarget>>();
            mockCondition3.Setup(x => x.Test(It.IsAny<IEnumerable<DummyTarget>>(), subject))
                .Returns(expectedTargets);
            rule.AddCondition(mockCondition3.Object);


            rule.Test(world, subject);

            mockAction.Verify(x => x.Perform(expectedTargets, subject), Times.Once);
        }
    }
}
