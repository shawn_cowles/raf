﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Ploeh.AutoFixture;

namespace RAF.Test
{
    [TestClass]
    public class RulesetTest
    {
        private Fixture _fixture = new Fixture();

        [TestMethod]
        public void Ruleset_calls_Test_on_its_rules()
        {
            var mockRule = new Mock<Rule<DummyWorld, DummySubject>>();
            mockRule.Setup(x=>x.Test(It.IsAny<DummyWorld>(), It.IsAny<DummySubject>()))
                .Returns(new RuleResult(
                    _fixture.Create<string>(),
                    _fixture.Create<string>(),
                    _fixture.Create<bool>()));

            var setName = _fixture.Create<string>();

            var ruleSet = new Ruleset<Rule<DummyWorld, DummySubject>, DummyWorld, DummySubject>(setName);
            ruleSet.AddRule(mockRule.Object);

            var world = _fixture.Create<DummyWorld>();
            var subject = _fixture.Create<DummySubject>();

            ruleSet.TestRules(world, subject);

            mockRule.Verify(x => x.Test(world, subject), Times.AtLeastOnce);
        }

        [TestMethod]
        public void Test_not_called_on_rules_added_after_passing_rule()
        {
            var firstRule = new Mock<Rule<DummyWorld, DummySubject>>();
            firstRule.Setup(x => x.Test(It.IsAny<DummyWorld>(), It.IsAny<DummySubject>()))
                .Returns(new RuleResult(
                    _fixture.Create<string>(),
                    _fixture.Create<string>(),
                    false));
            var secondRule = new Mock<Rule<DummyWorld, DummySubject>>();
            secondRule.Setup(x => x.Test(It.IsAny<DummyWorld>(), It.IsAny<DummySubject>()))
                .Returns(new RuleResult(
                    _fixture.Create<string>(),
                    _fixture.Create<string>(),
                    true));
            var thirdRule = new Mock<Rule<DummyWorld, DummySubject>>();
            thirdRule.Setup(x => x.Test(It.IsAny<DummyWorld>(), It.IsAny<DummySubject>()))
                .Returns(new RuleResult(
                    _fixture.Create<string>(),
                    _fixture.Create<string>(),
                    false));

            var setName = _fixture.Create<string>();

            var ruleSet = new Ruleset<Rule<DummyWorld, DummySubject>, DummyWorld, DummySubject>(setName);
            ruleSet.AddRule(firstRule.Object);
            ruleSet.AddRule(secondRule.Object);
            ruleSet.AddRule(thirdRule.Object);

            var world = _fixture.Create<DummyWorld>();
            var subject = _fixture.Create<DummySubject>();

            ruleSet.TestRules(world, subject);

            firstRule.Verify(x => x.Test(world, subject), Times.AtLeastOnce);
            secondRule.Verify(x => x.Test(world, subject), Times.AtLeastOnce);
            thirdRule.Verify(x => x.Test(world, subject), Times.Never);
        }

        [TestMethod]
        public void Test_returns_rule_results_in_ruleset_result()
        {
            var firstResult = new RuleResult(_fixture.Create<string>(), _fixture.Create<string>(), false);
            var secondResult = new RuleResult(_fixture.Create<string>(), _fixture.Create<string>(), true);

            var firstRule = new Mock<Rule<DummyWorld, DummySubject>>();
            firstRule.Setup(x => x.Test(It.IsAny<DummyWorld>(), It.IsAny<DummySubject>()))
                .Returns(firstResult);
            var secondRule = new Mock<Rule<DummyWorld, DummySubject>>();
            secondRule.Setup(x => x.Test(It.IsAny<DummyWorld>(), It.IsAny<DummySubject>()))
                .Returns(secondResult);

            var setName = _fixture.Create<string>();

            var ruleSet = new Ruleset<Rule<DummyWorld, DummySubject>, DummyWorld, DummySubject>(setName);
            ruleSet.AddRule(firstRule.Object);
            ruleSet.AddRule(secondRule.Object);

            var world = _fixture.Create<DummyWorld>();
            var subject = _fixture.Create<DummySubject>();

            var results = ruleSet.TestRules(world, subject);

            Assert.IsTrue(results.RuleResults.Contains(firstResult));
            Assert.IsTrue(results.RuleResults.Contains(secondResult));
        }

        [TestMethod]
        public void Test_returns_ruleset_name_in_ruleset_result()
        {
            var mockRule = new Mock<Rule<DummyWorld, DummySubject>>();
            mockRule.Setup(x => x.Test(It.IsAny<DummyWorld>(), It.IsAny<DummySubject>()))
                .Returns(new RuleResult(
                    _fixture.Create<string>(),
                    _fixture.Create<string>(),
                    _fixture.Create<bool>()));

            var setName = _fixture.Create<string>();

            var ruleSet = new Ruleset<Rule<DummyWorld, DummySubject>, DummyWorld, DummySubject>(setName);
            ruleSet.AddRule(mockRule.Object);

            var world = _fixture.Create<DummyWorld>();
            var subject = _fixture.Create<DummySubject>();

            var result = ruleSet.TestRules(world, subject);

            Assert.AreEqual(setName, result.RulesetName);
        }

        [TestMethod]
        public void Test_returns_subject_in_ruleset_result()
        {
            var mockRule = new Mock<Rule<DummyWorld, DummySubject>>();
            mockRule.Setup(x => x.Test(It.IsAny<DummyWorld>(), It.IsAny<DummySubject>()))
                .Returns(new RuleResult(
                    _fixture.Create<string>(),
                    _fixture.Create<string>(),
                    _fixture.Create<bool>()));

            var setName = _fixture.Create<string>();

            var ruleSet = new Ruleset<Rule<DummyWorld, DummySubject>, DummyWorld, DummySubject>(setName);
            ruleSet.AddRule(mockRule.Object);

            var world = _fixture.Create<DummyWorld>();
            var subject = _fixture.Create<DummySubject>();

            var result = ruleSet.TestRules(world, subject);

            Assert.AreEqual(subject, result.Subject);
        }
    }
}
