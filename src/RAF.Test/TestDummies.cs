﻿using System;
using System.Collections.Generic;
using RAF;

namespace RAF.Test
{
    public class DummyWorld : Source<DummyTarget>
    {
        public IEnumerable<DummyTarget> Targets { get; set; }

        public virtual IEnumerable<DummyTarget> GetTargets()
        {
            return Targets;
        }
    }

    public class DummyTarget
    {
    }

    public class DummySubject
    {
    }
}