﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace RAF
{
    /// <summary>
    /// A composite rule that operates on a list of targets, filtering the list with conditions
    /// before reaching a set that is then operated on by an action. This rule fails if the list of
    /// targets is empty at any time.
    /// </summary>
    /// <typeparam name="W">The type of source for targets.</typeparam>
    /// <typeparam name="S">The type of subject this rule operates on.</typeparam>
    /// <typeparam name="T">The type of target this rule operates on.</typeparam>
    public class CompositeRule<W, S, T> : Rule<W, S> where W : Source<T>
    {
        private ICompositeRuleAction<S, T> _action;

        private string _ruleName;

        private List<ICompositeRuleCondition<S, T>> _conditions;

        /// <summary>
        /// Create a new composite rule.
        /// </summary>
        /// <param name="ruleName">The name of the rule (used for logging).</param>
        /// <param name="action">The action this rule performs when it passes.</param>
        public CompositeRule(string ruleName, ICompositeRuleAction<S, T> action)
        {
            _ruleName = ruleName;

            _action = action;

            _conditions = new List<ICompositeRuleCondition<S, T>>();
        }

        /// <summary>
        /// Add a condition to the end of this composite rule.
        /// </summary>
        /// <param name="newCondition">The condition to add.</param>
        public void AddCondition(ICompositeRuleCondition<S, T> newCondition)
        {
            _conditions.Add(newCondition);
        }

        /// <summary>
        /// Test this composite rule.
        /// </summary>
        /// <param name="source">The target source.</param>
        /// <param name="subject">The subject of this test.</param>
        /// <returns>The result of this test, indicating pass or failure.</returns>
        public override RuleResult Test(W source, S subject)
        {
            var targets = source.GetTargets();

            if (!targets.Any())
            {
                return new RuleResult(_ruleName, "Source contained no targets.", false);
            }

            foreach (var condition in _conditions)
            {
                targets = condition.Test(targets, subject);

                if (!targets.Any())
                {
                    return new RuleResult(_ruleName, condition.FriendlyName, false);
                }
            }

            _action.Perform(targets, subject);

            return new RuleResult(_ruleName, "", true);
        }

        /// <summary>
        /// Test this rule, skipping any logging and running faster.
        /// </summary>
        /// <param name="source">The target source.</param>
        /// <param name="subject">The subject of this rule test.</param>
        /// <returns>True if the rule passed, false if it did not.</returns>
        public override bool TestQuickly(W source, S subject)
        {
            var targets = source.GetTargets();

            if (!targets.Any())
            {
                return false;
            }

            foreach (var condition in _conditions)
            {
                targets = condition.Test(targets, subject);
            }

            targets = targets.ToArray();

            if(!targets.Any())
            {
                return false;
            }

            _action.Perform(targets, subject);

            return true;
        }
    }
}
