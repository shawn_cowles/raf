﻿using System.Collections.Generic;

namespace RAF
{
    /// <summary>
    /// An interface for the action of a CompositeRule. This action will be called if the rule
    /// passes.
    /// </summary>
    /// <typeparam name="S">The type of subject this action operates on.</typeparam>
    /// <typeparam name="T">The type of target this action operates on.</typeparam>
    public interface ICompositeRuleAction<S, T>
    {
        /// <summary>
        /// Perform the action with given subject and targets.
        /// </summary>
        /// <param name="targets">The targets for the action.</param>
        /// <param name="subject">The subject performing the action.</param>
        void Perform(IEnumerable<T> targets, S subject);
    }
}