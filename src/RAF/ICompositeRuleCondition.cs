﻿using System.Collections.Generic;

namespace RAF
{
    /// <summary>
    /// An interface for a condition of a CompositeRule. This condition will be tested to determine
    /// if the rule passed.
    /// </summary>
    /// <typeparam name="S">The type of subject this condition operates on.0</typeparam>
    /// <typeparam name="T">The type of target this condition operates on.</typeparam>
    public interface ICompositeRuleCondition<S, T>
    {
        /// <summary>
        /// The friendly name of this condition, used for logging.
        /// </summary>
        string FriendlyName { get; }

        /// <summary>
        /// Test this condition on the given targets and subject.
        /// </summary>
        /// <param name="targets">The targets to test this condition on.</param>
        /// <param name="subject">The subject to test this condition with.</param>
        /// <returns>An IEnumerable of targets that pass this condition. Will usually but not always
        /// contain a subset of the targets passed in.</returns>
        IEnumerable<T> Test(IEnumerable<T> targets, S subject);
    }
}
