﻿namespace RAF
{
    /// <summary>
    /// An abstract rule, tested by a ruleset to decide on an action.
    /// </summary>
    /// <typeparam name="W">The type of world this rule operates on.</typeparam>
    /// <typeparam name="S">the type of subject this rule operates on.</typeparam>
    public abstract class Rule<W, S>
    {
        /// <summary>
        /// Test this rule.
        /// </summary>
        /// <param name="world">The world containing the subject.</param>
        /// <param name="subject">The subject of this rule test.</param>
        /// <returns>The result of the test, including pass/fail, and the reason.</returns>
        public abstract RuleResult Test(W world, S subject);

        /// <summary>
        /// Test this rule, skipping any logging and running faster.
        /// </summary>
        /// <param name="world">The world containing the subject.</param>
        /// <param name="subject">The subject of this rule test.</param>
        /// <returns>True if the rule passed, false if it did not.</returns>
        public abstract bool TestQuickly(W world, S subject);
    }
}
