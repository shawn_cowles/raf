﻿namespace RAF
{
    /// <summary>
    /// Contains the results of testing a single Rule
    /// </summary>
    public class RuleResult
    {
        /// <summary>
        /// The name of the Rule tested.
        /// </summary>
        public string RuleName { get; private set; }

        /// <summary>
        /// The reason for the rule passing or failing.
        /// </summary>
        public string Reason { get; private set; }

        /// <summary>
        /// True if the rule passed the test.
        /// </summary>
        public bool Passed { get; private set; }

        /// <summary>
        /// Construct a new RuleResult.
        /// </summary>
        /// <param name="ruleName">The name of the rule that was tested.</param>
        /// <param name="reason">The reason for the result.</param>
        /// <param name="passed">The pass/fail result of the test.</param>
        public RuleResult(string ruleName, string reason, bool passed)
        {
            RuleName = ruleName;

            Reason = reason;

            Passed = passed;
        }
    }
}
