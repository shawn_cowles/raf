﻿using System.Collections.Generic;

namespace RAF
{
    /// <summary>
    /// A set of rules to be tested.
    /// </summary>
    /// <typeparam name="R">The type of rules contained within this ruleset.</typeparam>
    /// <typeparam name="W">The type of world this ruleset operates on.</typeparam>
    /// <typeparam name="S">The type of subject this ruleset operates on.</typeparam>
    public class Ruleset <R, W, S> where R : Rule<W, S>
    {
        private List<R> _rules;

        private string _setName;

        /// <summary>
        /// Create a new Ruleset.
        /// </summary>
        /// <param name="setName">The name of this ruleset (used for logging)</param>
        public Ruleset(string setName)
        {
            _rules = new List<R>();

            _setName = setName;
        }

        /// <summary>
        /// Add a rule to this ruleset.
        /// </summary>
        /// <param name="newRule">The rule to add.</param>
        public void AddRule(R newRule)
        {
            _rules.Add(newRule);
        }

        /// <summary>
        /// Test this ruleset against a world and subject.
        /// </summary>
        /// <param name="world">The world containing the subject.</param>
        /// <param name="subject">The subject of this rule test.</param>
        /// <returns>The result of each of the tested rules, as well as information on the ruleset itself.</returns>
        public RulesetResult<S> TestRules(W world, S subject)
        {
            var rulesetResult = new RulesetResult<S>(_setName, subject);

            foreach(var rule in _rules)
            {
                var ruleResult = rule.Test(world, subject);

                rulesetResult.RuleResults.Add(ruleResult);

                if(ruleResult.Passed)
                {
                    return rulesetResult;
                }
            }

            return rulesetResult;
        }

        /// <summary>
        /// Test this ruleset against a world and subject. This skips any logging, and will run faster.
        /// </summary>
        /// <param name="world">The world containing the subject.</param>
        /// <param name="subject">The subject of this rule test.</param>
        public void TestRulesQuickly(W world, S subject)
        {
            foreach (var rule in _rules)
            {
                var rulePassed = rule.TestQuickly(world, subject);
                
                if (rulePassed)
                {
                    return;
                }
            }
        }
    }
}
