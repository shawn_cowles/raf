﻿using System.Collections.Generic;
namespace RAF
{
    /// <summary>
    /// Contains the results of testing a Ruleset.
    /// </summary>
    /// <typeparam name="S">The type of the subject tested.</typeparam>
    public class RulesetResult<S>
    {
        /// <summary>
        /// The name of the set tested.
        /// </summary>
        public string RulesetName { get; private set; }

        /// <summary>
        /// The subject that was tested.
        /// </summary>
        public S Subject { get; private set; }

        /// <summary>
        /// The individual results of each of the Rules tested.
        /// </summary>
        public List<RuleResult> RuleResults { get; private set; }
        
        /// <summary>
        /// Construct a new RulesetResult with the given set name and subject.
        /// </summary>
        /// <param name="ruleSetName">The name of the set tested.</param>
        /// <param name="subject">The subject tested.</param>
        public RulesetResult(string ruleSetName, S subject)
        {
            RulesetName = ruleSetName;
            Subject = subject;

            RuleResults = new List<RuleResult>();
        }
    }
}
