﻿using System.Collections.Generic;

namespace RAF
{
    /// <summary>
    /// An interface defining a source for a composite rule.
    /// </summary>
    /// <typeparam name="T">The type of target returned by this source.</typeparam>
    public interface Source<T>
    {
        /// <summary>
        /// Get the initial list of targets for processing.
        /// </summary>
        /// <returns>The initial list of targets for processing.</returns>
        IEnumerable<T> GetTargets();
    }
}
